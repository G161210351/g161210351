﻿using System.Web;
using System.Web.Mvc;

namespace WebProgramlama_Odev
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
